package com.example.emailserviceTest.controllerTest;

import static org.assertj.core.api.Assertions.assertThat;

import emailservice.controller.EmailController;
import emailservice.model.Email;
import emailservice.repository.EmailRepository;
import emailservice.service.EmailService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.server.ResponseStatusException;

class EmailControllerTest {

    private final EmailRepository emailRepository = new InMemoryEmailRepository();
    private EmailService emailService;
    private EmailController emailController;

    @BeforeEach
    void setUp() {
        emailService = new EmailService((JavaMailSender) emailRepository);
        emailController = new EmailController(emailService);
    }

    @Test
    void testSendEmail() {
        String subject = "Test Subject";
        String text = "Test Text";
        List<String> emailList = new ArrayList<>();
        emailList.add("test1@test.com");
        emailList.add("test2@test.com");
        emailList.add("test3@test.com");
        ResponseEntity<Void> response = emailController.sendEmail(subject, text);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void testGetAllEmails() {
        Email email1 = new Email();
        email1.setSubject("Test Subject 1");
        email1.setText("Test Text 1");

        Email email2 = new Email();
        email2.setSubject("Test Subject 2");
        email2.setText("Test Text 2");

        emailRepository.save(email1);
        emailRepository.save(email2);

        List<Email> response = emailController.getAllEmails();

        assertThat(response.size()).isEqualTo(2);
        assertThat(response.get(0).getSubject()).isEqualTo(email1.getSubject());
        assertThat(response.get(0).getText()).isEqualTo(email1.getText());
        assertThat(response.get(1).getSubject()).isEqualTo(email2.getSubject());
        assertThat(response.get(1).getText()).isEqualTo(email2.getText());
    }

    @Test
    void testGetEmailById() {
        Email email = new Email();
        email.setSubject("Test Subject");
        email.setText("Test Text");

        emailRepository.save(email);

        ResponseEntity<Email> response = emailController.getEmailById(email.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getId()).isEqualTo(email.getId());
        assertThat(response.getBody().getSubject()).isEqualTo(email.getSubject());
        assertThat(response.getBody().getText()).isEqualTo(email.getText());
    }

    @Test
    void testGetEmailByInvalidId() {
        Long id = 999L;

        try {
            emailController.getEmailById(id);
        } catch (ResponseStatusException ex) {
            assertThat(ex.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
            assertThat(ex.getReason()).isEqualTo("Email not found for id: " + id);
        }
    }

    private static class InMemoryEmailRepository implements EmailRepository {
        private final List<Email> emailList = new ArrayList<>();
        private long id = 1L;

        @Override
        public <S extends Email> S save(S entity) {
            entity.setId(id++);
            emailList.add(entity);
            return entity;
        }

        @Override
        public List<Email> findAll() {
            return emailList;
        }

        @Override
        public List<Email> findAll(Sort sort) {
            return null;
        }

        @Override
        public Page<Email> findAll(Pageable pageable) {
            return null;
        }

        @Override
        public List<Email> findAllById(Iterable<Long> longs) {
            return null;
        }

        @Override
        public long count() {
            return 0;
        }

        @Override
        public <S extends Email> List<S> saveAll(Iterable<S> entities) {
            return null;
        }

        @Override
        public void flush() {

        }

        @Override
        public <S extends Email> S saveAndFlush(S entity) {
            return null;
        }

        @Override
        public <S extends Email> List<S> saveAllAndFlush(Iterable<S> entities) {
            return null;
        }

        @Override
        public void deleteAllInBatch(Iterable<Email> entities) {

        }

        @Override
        public void deleteAllByIdInBatch(Iterable<Long> longs) {

        }

        @Override
        public void deleteAllInBatch() {

        }

        @Override
        public Email getOne(Long aLong) {
            return null;
        }

        @Override
        public Email getById(Long aLong) {
            return null;
        }

        @Override
        public <S extends Email> Optional<S> findOne(Example<S> example) {
            return Optional.empty();
        }

        @Override
        public <S extends Email> List<S> findAll(Example<S> example) {
            return null;
        }

        @Override
        public <S extends Email> List<S> findAll(Example<S> example, Sort sort) {
            return null;
        }

        @Override
        public <S extends Email> Page<S> findAll(Example<S> example, Pageable pageable) {
            return null;
        }

        @Override
        public <S extends Email> long count(Example<S> example) {
            return 0;
        }

        @Override
        public <S extends Email> boolean exists(Example<S> example) {
            return false;
        }

        @Override
        public <S extends Email, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
            return null;
        }

        @Override
        public Optional<Email> findById(Long aLong) {
            return emailList.stream()
                    .filter(email -> email.getId().equals(aLong))
                    .findFirst();
        }

        @Override
        public boolean existsById(Long aLong) {
            return false;
        }

        @Override
        public void deleteById(Long id) {
            emailList.removeIf(email -> email.getId().equals(id));
        }

        @Override
        public void delete(Email entity) {

        }

        @Override
        public void deleteAllById(Iterable<? extends Long> longs) {

        }

        @Override
        public void deleteAll(Iterable<? extends Email> entities) {

        }

        @Override
        public void deleteAll() {

        }
    }
}